#ifndef OBJECT_H_
#define OBJECT_H_

#include "functor.h"

//forward declared from console.h, first included in funtor.h
void printme(std::string);

#include <string>
#include <vector>
#include <iostream>
#include <unistd.h>

/* //wew
typedef enum {
	HELP, EXAMINE,
	PICKUP, DROP,
	SWITCH,
	EQUIP,
	CONSUME,
} ACT_FLAG; */

struct ACT_NODE{
	std::string ACT;
	functor* EXCECUTE;
};

class object {

	private:
		std::vector<ACT_NODE*> _map; 
		
	public:
		std::string name; 
		int goodie;
	
		object(std::string NameArg, std::string descript_arg){
			name = NameArg;
			this->AddNode("examine", new ExamineFunctor(this, descript_arg));
		}
		
		void AddNode(std::string key, functor* action){
			
			ACT_NODE* meme = new ACT_NODE();
			meme->ACT = key;
			meme->EXCECUTE = action;
			_map.push_back(meme);
		}
		
		void ExcecuteNode(std::string key){
			for(ACT_NODE* i : _map){
				if (i->ACT == key){
					i->EXCECUTE->excecute();
					return;
				}
			}
			
			printme("couldn't " + key + " the " + name);
			usleep(PAUSETIME);
		}

};




#endif
