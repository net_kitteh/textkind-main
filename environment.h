#ifndef ENV
#define ENV

#include <string>
#include <vector>

//forwards declaration
class object;

class environment{
	public:
		std::string name;
		std::string description;
		std::vector<object*> contents;
		
		//returns the prompt for the env
		std::string prompt(){
			std::string pr = name + " \n \n" + description + "\n\n ENVIRONMENT\n [";			
			return pr;
		}
};



#endif
