
#ifndef FUNCT
#define FUNCT

#include "console.h"
#include "environment.h"
#include <string>

#include <iostream>

//forward declaration
class object;
class ACT_NODE;

class functor {
	
	public:
		object* _self;	
	
		functor(object* arg){
			_self = arg;
		}
		
		virtual void excecute(){};
};

class ExamineFunctor : public functor {
	
	private:
		std::string _str;
	
	public:
		ExamineFunctor(object* am, std::string ex) : functor(am) {
			_str = ex;
		}
		
		void excecute() {
			printme(_str);
			usleep(PAUSETIME);
		}
};

class PickupFunctor : public functor {
	
	public:
		PickupFunctor(object* arg) : functor(arg){}
		
		void excecute() {
		
			int index;
			
			//find in evironment
			bool found = false;
			for(int i = 0; i < worldmap::current->contents.size(); ++i){
				object* o = worldmap::current->contents[i];
				
				if(o == this->_self){
					found = true;
					index = i;
				}
			}
			
			//break if object not in environemtn
			if(!found){
				printme("object is not in the environment");
				usleep(PAUSETIME);
				return;
			}
				
			//swap places
			player::inventory.push_back(_self);
			worldmap::current->contents.erase(worldmap::current->contents.begin() + index);
			return;
		}
	
};

class DropFunctor : public functor {
	
	public:
		DropFunctor(object* arg) : functor(arg){}
		
		void excecute() {
		
			int index;
			
			//find in evironment
			bool found = false;
			for(int i = 0; i < player::inventory.size(); ++i){
				object* o = player::inventory[i];
				
				if(o == this->_self){
					found = true;
					index = i;
				}
			}
			
			//break if object not in inventory
			if(!found){
				printme("object is not in your inventory");
				usleep(PAUSETIME);
				return;
			}
				
			//swap places
			worldmap::current->contents.push_back(_self);
			player::inventory.erase(player::inventory.begin() + index);
			return;
		}
	
};

class DoorFunctor : public functor {
	
	private:
		object* _key;
		environment* _leadsTo;
		bool _isLocked;
		
	public:
		DoorFunctor(object* self_, environment* leadsTo_, object* key_, bool initialLockState) : functor(self_) {
			_key = key_;
			_leadsTo = leadsTo_;
			_isLocked = initialLockState;
		}
	
		void excecute() {
			//check lock status, then check key
			//exit if player doesnt have the right key
			
			//check lock
			if(_isLocked){
			
				//check for key
				bool keyfound = false;
				int keyindex;
				
				for(int i = 0; i < player::inventory.size(); ++i){
					object* o = player::inventory[i];
				
					if(o == _key){
						keyfound = true;
					}
					
					keyindex = i;
				}
				
				if(keyfound){
					//player had the key
					//unlock
					_isLocked = false;
					//remove key
					player::inventory.erase(player::inventory.begin() + keyindex);
				} else {
					//player didnt have key
					printme("you dont have the right key");
					usleep(PAUSETIME);
					
					//exit
					return;
				}
			} 
			
			//OPEN THE DOOR
			worldmap::current = _leadsTo;
		}
};

//I need a switch one that simply flips a switch to false or smth

#endif
