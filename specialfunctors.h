#ifndef SPC_FUNC
#define SPC_FUNC

#include "object.h"
#include "functor.h"
#include "environment.h"

class CameraFunctor : public functor {

	private:
		object* _picture;
	
	public:
		CameraFunctor(object* arg) : functor(arg){
			//lolhehe
		}
		
		void excecute(){
			
			//TODO: camera relies on film?
		
			//GEN PICTURE OBJECT
			std::string objname = "picture_" + worldmap::current->name;
			std::string objdesc = "Picture of " + worldmap::current->name;
			_picture = new object(objname, objdesc);
			
			//ATTATCH PIC ATTRIBUTES
			_picture->AddNode("take", new PickupFunctor( _picture ) );
			_picture->AddNode("drop", new DropFunctor( _picture ) );
			_picture->AddNode("view", new DoorFunctor( _picture, worldmap::current, nullptr, false ) );
			
			//ADD TO INVENTORY
			player::inventory.push_back(_picture);
		}
};

//i need a switch that drops an object
class DispenceItemFunctor : public functor {
	private:
		object* droppable;
	
	public:
		DispenceItemFunctor(object* ARG, object* drop_goodie) : functor(ARG) {
			droppable = drop_goodie;
		}
		
		void excecute(){
			worldmap::current->contents.push_back( droppable );
		}

};



#endif
